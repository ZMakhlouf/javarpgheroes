/**
 * Class representing armors and their type in the RPGheroes console application.
 *
 * @author [Zakaria Makhlouf]
 * @version 1.0
 */
package equipment.armor;

import equipment.item.Item;
import heroes.HeroAttribute;

public class Armor extends Item {
    private final ArmorType armorType;
    private final HeroAttribute armorAttribute;

    /**
     * Constructor for the Armor class.
     *
     * @param name The name of the armor.
     * @param requiredLevel The level required to use the armor.
     * @param armorType The type of armor.
     * @param armorAttribute The Hero attribute that the armor carries.
     * @param slot The equipment slot the armor can be equipped to.
     */
    public Armor(String name, int requiredLevel, ArmorType armorType, HeroAttribute armorAttribute, Slot slot) { // << HeroAttribute armorAttribute
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.armorAttribute = armorAttribute;
    }

    /**
     * Returns the type of the armor.
     *
     * @return An ArmorType enumeration value representing the type of the armor.
     */
    public ArmorType getArmorType() {
        return armorType;
    }

    /**
     * Returns the Hero attribute that the armor carries(STR, DEX, INT).
     *
     * @return A HeroAttribute enumeration value representing the attribute the armor carries.
     */
    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }

    /**
     * Enumeration representing the different types of armor.
     */
    public enum ArmorType {
        Cloth, Leather, Mail, Plate
    }
}
