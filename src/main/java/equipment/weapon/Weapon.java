/**
 * Class representing the weapons in the console application.
 *
 * @author [Zakaria Makhlouf]
 * @version 1.0
 */
package equipment.weapon;

import equipment.item.Item;

public class Weapon extends Item {
    private final WeaponType weaponType;
    private final int weaponDamage;

    /**
     * Constructor for the Weapon class.
     *
     * @param name The name of the weapon.
     * @param requiredLevel The level required to use the weapon.
     * @param weaponType The type of weapon.
     * @param weaponDamage The damage dealt by the weapon.
     */
    public Weapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage) {
        super(name, requiredLevel, Slot.Weapon);
        this.weaponType = weaponType;
        this.weaponDamage = weaponDamage;
    }

    /**
     * Returns the type of weapon.
     *
     * @return A WeaponType enumeration value representing the type of weapon.
     */
    public WeaponType getWeaponType() {
        return weaponType;
    }

    /**
     * Returns the damage dealt by the weapon.
     *
     * @return An integer representing the damage dealt by the weapon.
     */
    public int getWeaponDamage() {
        return weaponDamage;
    }

    /**
     * Enumeration representing the different types of weapons.
     */
    public enum WeaponType {
        Axe, Bow, Dagger, Hammer, Staff, Sword, Wand
    }
}
