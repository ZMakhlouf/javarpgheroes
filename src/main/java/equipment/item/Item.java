/**
 * Abstract class representing an item for the heroes in the console application.
 *
 * @author [Zakaria Makhlouf]
 * @version 1.0
 */
package equipment.item;

public abstract class Item {
    private final String name;
    private final int requiredLevel;
    private Slot slot;

    /**
     * Constructor for the Item class.
     *
     * @param name The name of the item.
     * @param requiredLevel The level required to use the item.
     * @param slot The equipment slot the item can be equipped to.
     */
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    /**
     * Returns the name of the item.
     *
     * @return A string representing the name of the item.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the required level to use the item.
     *
     * @return An integer representing the required level.
     */
    public int getRequiredLevel() {
        return requiredLevel;
    }

    /**
     * Returns the slot the item can be equipped to.
     *
     * @return A Slot enumeration value representing the equipment slot.
     */
    public Slot getSlot() {
        return slot;
    }

    /**
     * Enumeration representing the different equipment slots an item can be equipped to.
     */
    public enum Slot {
        Weapon, Head, Body, Legs
    }

}
