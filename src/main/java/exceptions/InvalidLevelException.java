/**
 * Exception thrown when an invalid level is encountered.
 *
 * @author [Zakaria Makhlouf]
 * @version 1.0
 */
package exceptions;

public class InvalidLevelException extends Exception {

    /**
     * Constructor for InvalidLevelException.
     *
     * @param message The message to be displayed when the exception is thrown.
     */
    public InvalidLevelException(String message) {
        super(message);
    }
}
