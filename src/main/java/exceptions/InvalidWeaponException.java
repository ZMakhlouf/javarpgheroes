/**
 * Exception thrown when an invalid weapon is equipped.
 *
 * @author [Zakaria Makhlouf]
 * @version 1.0
 */
package exceptions;

public class InvalidWeaponException extends Exception {

    /**
     * Constructor for InvalidWeaponException.
     *
     * @param message The message to be displayed when the exception is thrown.
     */
    public InvalidWeaponException(String message) {
        super(message);
    }
}

