/**
 * Exception thrown when an invalid armor is equipped.
 *
 * @author [Zakaria Makhlouf]
 * @version 1.0
 */
package exceptions;

public class InvalidArmorException extends Exception {

    /**
     * Constructor for InvalidArmorException.
     *
     * @param message The message to be displayed when the exception is thrown.
     */
    public InvalidArmorException(String message) {
        super(message);
    }
}
