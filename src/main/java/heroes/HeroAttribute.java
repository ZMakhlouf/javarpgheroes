/**
 * Class representing the attributes of a hero in the console application.
 *
 * @author [Zakaria Makhlouf]
 * @version 1.0
 */
package heroes;

public class HeroAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    /**
     * Constructor for the HeroAttribute class.
     *
     * @param strength The strength attribute of the hero.
     * @param dexterity The dexterity attribute of the hero.
     * @param intelligence The intelligence attribute of the hero.
     */
    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Returns the strength attribute of the hero.
     *
     * @return An integer representing the strength attribute of the hero.
     */
    public int getStrength() {
        return strength;
    }

    /**
     * Returns the dexterity attribute of the hero.
     *
     * @return An integer representing the dexterity attribute of the hero.
     */
    public int getDexterity() {
        return dexterity;
    }

    /**
     * Returns the intelligence attribute of the hero.
     *
     * @return An integer representing the intelligence attribute of the hero.
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Increases the attributes of the hero by the attributes of the armorAttribute.
     *
     * @param otherHeroAttribute The attributes of the armor to be added to the current hero's attributes.
     */
    public void increase(HeroAttribute otherHeroAttribute) {
        this.strength += otherHeroAttribute.strength;
        this.dexterity += otherHeroAttribute.dexterity;
        this.intelligence += otherHeroAttribute.intelligence;
    }


}
