/**
 * Class representing a Warrior hero in the console application.
 *
 * @author [Zakaria Makhlouf]
 *
 * @version 1.0
 */
package heroes;

import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;

import java.util.ArrayList;

public class Warrior extends Hero {
    /**
     * The base Strength level of the warrior Hero.
     */
    private static final int START_STRENGTH = 5;
    /**
     * The base Dexterity level of the warrior Hero.
     */
    private static final int START_DEXTERITY = 2;
    /**
     * The base Intelligence level of the warrior Hero.
     */
    private static final int START_INTELLIGENCE = 1;
    /**
     * The increase of the Strength HeroAttribute upon gaining 1 level for the warrior.
     */
    private static final int STRENGTH_INCREASE_PER_LEVEL = 3;
    /**
     * The increase of the Dexterity HeroAttribute upon gaining 1 level for the warrior.
     */
    private static final int DEXTERITY_INCREASE_PER_LEVEL = 2;
    /**
     * The increase of the Intelligence HeroAttribute upon gaining 1 level for the warrior.
     */
    private static final int INTELLIGENCE_INCREASE_PER_LEVEL = 1;
    /**
     * The total increase of HeroAttributes for the hero Warrior.
     */
    private static final HeroAttribute INCREASE_HERO_ATTRIBUTES = new HeroAttribute(STRENGTH_INCREASE_PER_LEVEL, DEXTERITY_INCREASE_PER_LEVEL, INTELLIGENCE_INCREASE_PER_LEVEL);

    /**
     * Constructor for the Warrior class.
     *
     * @param name The name of the warrior.
     * Super - Adds the name from the abstract Hero class.
     * Super - Changes the Hero Attributes to the warrior specific StartingHeroAttributes.
     * validWeaponTypes checks if warrior equips the appropriate weapons (Dagger, Sword) otherwise throws InvalidWeaponException.
     * ValidArmorTypes checks if warrior equips the appropriate armorType(Leather, Mail) otherwise throws InvalidArmorException.
     */
    public Warrior(String name) {
        super(name, new HeroAttribute(START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE));
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(Weapon.WeaponType.Axe);
        this.validWeaponTypes.add(Weapon.WeaponType.Sword);
        this.validWeaponTypes.add(Weapon.WeaponType.Hammer);
        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(Armor.ArmorType.Plate);
        this.validArmorTypes.add(Armor.ArmorType.Mail);
    }

    /**
     * Increase the level of the warrior by 1, and increase the HeroAttributes accordingly.
     */
    @Override
    public void levelUp() {
        // Call levelUp from Hero class
        super.levelUp();
        this.levelAttributes.increase(INCREASE_HERO_ATTRIBUTES);
    }

    /**
     * Returns the damage dealt by the warrior.
     *
     * @return A double representing the damage dealt by the warrior.
     * If the Hero is fighting without a weapon equipped -- throws exception message.
     */
    @Override
    public double calculateDamage() {
        int weaponDamage;
        try{
            weaponDamage = ((Weapon) equipment.get(Item.Slot.Weapon)).getWeaponDamage();
        } catch(Exception e){
            System.out.println("Your enemy will catch these hands because you do not have a weapon equipped.");
            weaponDamage = 1;
        }
        double damageAttribute = totalAttributes().getStrength();
        return weaponDamage * (1 + (damageAttribute / 100));
    }


}
