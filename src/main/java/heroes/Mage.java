/**
 * Class representing a Mage hero in a game or application.
 *
 * @author [Zakaria Makhlouf]
 *
 * @version 1.0
 */
package heroes;

import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;

import java.util.ArrayList;

public class Mage extends Hero {
    /**
     * The base Strength level of the mage Hero.
     */
    private static final int START_STRENGTH = 1;
    /**
     * The base Dexterity level of the mage Hero.
     */
    private static final int START_DEXTERITY = 1;
    /**
     * The base Intelligence level of the mage Hero.
     */
    private static final int START_INTELLIGENCE = 8;
    /**
     * The increase of the Strength HeroAttribute upon gaining 1 level for the mage.
     */
    private static final int STRENGTH_INCREASE_PER_LEVEL = 1;
    /**
     * The increase of the Dexterity HeroAttribute upon gaining 1 level for the mage.
     */
    private static final int DEXTERITY_INCREASE_PER_LEVEL = 1;
    /**
     * The increase of the Intelligence HeroAttribute upon gaining 1 level for the mage.
     */
    private static final int INTELLIGENCE_INCREASE_PER_LEVEL = 5;
    /**
     * The total increase of HeroAttributes for the hero Mage.
     */
    private static final HeroAttribute INCREASE_HERO_ATTRIBUTES = new HeroAttribute(STRENGTH_INCREASE_PER_LEVEL, DEXTERITY_INCREASE_PER_LEVEL, INTELLIGENCE_INCREASE_PER_LEVEL);

    /**
     * Constructor for the Mage class.
     *
     * @param name The name of the mage.
     * Super - Adds the name from the abstract Hero class.
     * Super - Changes the Hero Attributes to the Mage specific StartingHeroAttributes.
     * validWeaponTypes checks if mage equips the appropriate weapons (Staff, Wand) otherwise throws InvalidWeaponException.
     * ValidArmorTypes checks if mage equips the appropriate armorType(Cloth) otherwise throws InvalidArmorException.
     */
    public Mage(String name) {
        super(name, new HeroAttribute(START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE));
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(Weapon.WeaponType.Staff);
        this.validWeaponTypes.add(Weapon.WeaponType.Wand);
        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(Armor.ArmorType.Cloth);
    }

    /**
     * Increase the level of the mage by 1, and increase the attributes accordingly.
     */
    @Override
    public void levelUp() {
        // Call levelUp from Hero class
        super.levelUp();
        this.levelAttributes.increase(INCREASE_HERO_ATTRIBUTES);
    }

    /**
     * Returns the damage dealt by the mage.
     *
     * @return A double representing the damage dealt by the mage.
     * If the Hero is fighting without a weapon equipped -- throws exception message.
     */
    @Override
    public double calculateDamage() {
        int weaponDamage;
        try{
            weaponDamage = ((Weapon) equipment.get(Item.Slot.Weapon)).getWeaponDamage();
        } catch(Exception e){
            System.out.println("Your enemy will catch these hands because you do not have a weapon equipped.");
            weaponDamage = 1;
        }
        double damageAttribute = totalAttributes().getIntelligence();
        return weaponDamage * (1 + (damageAttribute / 100));
    }

}
