/**
 * Class representing a Rogue hero in the console application.
 *
 * @author [Zakaria Makhlouf]
 *
 * @version 1.0
 */
package heroes;

import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;

import java.util.ArrayList;

public class Rogue extends Hero {
    /**
     * The base Strength level of the rogue Hero.
     */
    private static final int START_STRENGTH = 2;
    /**
     * The base Dexterity level of the rogue Hero.
     */
    private static final int START_DEXTERITY = 6;
    /**
     * The base Intelligence level of the rogue Hero.
     */
    private static final int START_INTELLIGENCE = 1;
    /**
     * The increase of the Strength HeroAttribute upon gaining 1 level for the rogue.
     */
    private static final int STRENGTH_INCREASE_PER_LEVEL = 1;
    /**
     * The increase of the Dexterity HeroAttribute upon gaining 1 level for the rogue.
     */
    private static final int DEXTERITY_INCREASE_PER_LEVEL = 4;
    /**
     * The increase of the Intelligence HeroAttribute upon gaining 1 level for the rogue.
     */
    private static final int INTELLIGENCE_INCREASE_PER_LEVEL = 1;
    /**
     * The total increase of HeroAttributes for the hero Rogue.
     */
    private static final HeroAttribute INCREASE_HERO_ATTRIBUTES = new HeroAttribute(STRENGTH_INCREASE_PER_LEVEL, DEXTERITY_INCREASE_PER_LEVEL, INTELLIGENCE_INCREASE_PER_LEVEL);

    /**
     * Constructor for the Rogue class.
     *
     * @param name The name of the rogue.
     * Super - Adds the name from the abstract Hero class.
     * Super - Changes the Hero Attributes to the rogue specific StartingHeroAttributes.
     * validWeaponTypes checks if rogue equips the appropriate weapons (Dagger, Sword) otherwise throws InvalidWeaponException.
     * ValidArmorTypes checks if rogue equips the appropriate armorType(Leather, Mail) otherwise throws InvalidArmorException.
     */
    public Rogue(String name) {
        super(name, new HeroAttribute(START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE));
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(Weapon.WeaponType.Dagger);
        this.validWeaponTypes.add(Weapon.WeaponType.Sword);
        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(Armor.ArmorType.Leather);
        this.validArmorTypes.add(Armor.ArmorType.Mail);
    }

    /**
     * Increase the level of the rogue by 1, and increase the HeroAttributes accordingly.
     */
    @Override
    public void levelUp() {
        // Call levelUp from Hero class
        super.levelUp();
        this.levelAttributes.increase(INCREASE_HERO_ATTRIBUTES);
    }

    /**
     * Returns the damage dealt by the rogue.
     *
     * @return A double representing the damage dealt by the rogue.
     * If the Hero is fighting without a weapon equipped -- throws exception message.
     */
    @Override
    public double calculateDamage() {
        int weaponDamage;
        try{
            weaponDamage = ((Weapon) equipment.get(Item.Slot.Weapon)).getWeaponDamage();
        } catch(Exception e){
            System.out.println("Your enemy will catch these hands because you do not have a weapon equipped.");
            weaponDamage = 1;
        }
        double damageAttribute = totalAttributes().getDexterity();
        return weaponDamage * (1 + (damageAttribute / 100));
    }


}
