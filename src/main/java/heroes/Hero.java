/**
 * abstract class {@link Hero} represents a {@link Hero} in the RPG.
 *
 * @author [Zakaria Makhlouf]
 * @version 1.0
 * @since 13 January 2023
 */
package heroes;

import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;
import exceptions.InvalidArmorException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Hero {
    /**
     * Name of a {@link Hero}
     */
    protected String name;
    /**
     * Starting level of a {@link Hero}
     */
    protected static final int BASE_LEVEL = 1;
    /**
     * Current level of a {@link Hero}
     */
    protected int level;
    /**
     * The attributes of a {@link Hero} at the current level
     */
    protected HeroAttribute levelAttributes;
    /**
     * The equipment currently equipped by a {@link Hero}, stored in a {@link HashMap} with {@link Item.Slot} as the key, and {@link Item} as the value
     */
    protected HashMap<Item.Slot, Item> equipment = new HashMap<>();
    /**
     * List of valid weapon types for a {@link Hero}, and a List of valid armor types respectively
     */
    List<Weapon.WeaponType> validWeaponTypes;
    List<Armor.ArmorType> validArmorTypes;

    /**
     * Constructor for the {@link Hero} class
     *
     * @param name            The name of a {@link Hero}
     * @param levelAttributes The attributes of a {@link Hero} at the starting level
     *                        Initiate a {@link HashMap} declared equipment, and fill the empty {@link Item.Slot} with null values
     */
    public Hero(String name, HeroAttribute levelAttributes) {
        this.name = name;
        this.level = BASE_LEVEL;
        this.levelAttributes = levelAttributes;
        this.equipment.put(Item.Slot.Body, null);
        this.equipment.put(Item.Slot.Weapon, null);
        this.equipment.put(Item.Slot.Legs, null);
        this.equipment.put(Item.Slot.Head, null);
    }

    /**
     * Method of the type {@link HeroAttribute} that calculates and returns the total attributes of a Hero, including its ArmorAttributes
     *
     * @return A {@link HeroAttribute} object representing the total strength, dexterity, and intelligence of a {@link Hero
     */
    public HeroAttribute totalAttributes() {
        int totalStrength = levelAttributes.getStrength();
        int totalDexterity = levelAttributes.getDexterity();
        int totalIntelligence = levelAttributes.getIntelligence();
        for (Map.Entry<Item.Slot, Item> equipmentEntry : equipment.entrySet()) {
            if (equipmentEntry.getKey() != Item.Slot.Weapon) {
                Item item = equipmentEntry.getValue();
                if (item instanceof Armor armor) {
                    totalStrength += armor.getArmorAttribute().getStrength();
                    totalDexterity += armor.getArmorAttribute().getDexterity();
                    totalIntelligence += armor.getArmorAttribute().getIntelligence();
                }
            }
        }
        return new HeroAttribute(totalStrength, totalDexterity, totalIntelligence);
    }

    /**
     * Method to increase the level of a {@link Hero} with 1
     */
    public void levelUp() {
        level += 1;
    }

    /**
     * Method to let a {@link Hero} equip a Weapon that is suited for that {@link Hero}
     * The method checks whether the required level is met using an if-statement. If the required level is not met, {@link InvalidLevelException is thrown.
     * If the required level is met, the statement will then check whether the weapon type is a valid type for a specific {@link Hero}.
     * If the valid weapon type requirement is not met, {@link InvalidWeaponException} is thrown
     * If all the conditions are met, a {@link Hero} will equip its weapon in the {@link Item.Slot} for Weapon.
     *
     * @param weapon Weapon type object for the weapon to be equipped
     * @throws InvalidLevelException  if a Hero's level is insufficient to equip the weapon
     * @throws InvalidWeaponException if the weapon is not a valid weapon type for a Hero
     */
    public void equipWeapon(Weapon weapon) throws InvalidLevelException, InvalidWeaponException {
        boolean isRequiredLevel = this.level < weapon.getRequiredLevel();
        boolean isValidWeapon = validWeaponTypes.contains(weapon.getWeaponType());

        if (isRequiredLevel) {
            throw new InvalidLevelException("Your level is not high enough to equip this weapon.");
        } else if (!isValidWeapon) {
            throw new InvalidWeaponException("You cannot equip this type of weapon.");
        } else {
            equipment.put(Item.Slot.Weapon, weapon);
        }
    }

    /**
     * Method to check whether a Hero meets the necessary requirements to equip a specific {@link Weapon} suited for that Hero
     * * If the required level is met, the statement will then check whether the armor type is valid type for a specific Hero.
     * * If the valid armor type requirement is not met, InvalidWeaponException is thrown
     * * If all the conditions are met, a Hero will equip its weapon in the Item.Slot for Armor.
     *
     * @param armor Armor type object for the armor to be equipped
     * @throws InvalidArmorException if a Hero's level is insufficient to equip the Armor
     * @throws InvalidLevelException if the armor is not a valid armor type for a Hero
     */
    public void equipArmor(Armor armor) throws InvalidArmorException, InvalidLevelException {
        boolean isRequiredLevel = this.level < armor.getRequiredLevel();
        boolean isValidArmor = validArmorTypes.contains(armor.getArmorType());

        if (isRequiredLevel) {
            throw new InvalidLevelException("Your level is not high enough to equip this piece of armor.");
        } else if (!isValidArmor) {
            throw new InvalidArmorException("You cannot equip this type of armor.");
        } else {
            equipment.put(armor.getSlot(), armor);
        }
    }

    /**
     * abstract method to calculate a {@link Hero} his weapon damage, adding its damage attribute to the weapon damage
     *
     * @return weaponDamage * (1 + (damageAttribute / 100))
     */
    public abstract double calculateDamage();

    /**
     * Method to display all the statistics of a {@link Hero}
     * Using StringBuilder to append all the properties and attributes of the Hero
     * At last, print the display to the console
     *
     * @return
     */
    public String heroDisplay() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(name).append("\n");
        sb.append("Class: ").append(getClass().getSimpleName()).append("\n");
        sb.append("Level: ").append(level).append("\n");
        sb.append("Total Strength: ").append(totalAttributes().getStrength()).append("\n");
        sb.append("Total Dexterity: ").append(totalAttributes().getDexterity()).append("\n");
        sb.append("Total Intelligence: ").append(totalAttributes().getIntelligence()).append("\n");
        sb.append("Total Damage: ").append(calculateDamage()).append("\n\n");
//        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * Getter to return the name of a Hero
     *
     * @return name of Hero
     */
    public String getName() {
        return name;
    }

    /**
     * Getter to return the current level of a Hero
     *
     * @return current level of Hero
     */
    public int getLevel() {
        return level;
    }


}

