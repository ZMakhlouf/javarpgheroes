import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;
import exceptions.InvalidArmorException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;
import heroes.*;


public class Main {
    public static void main(String[] args) throws InvalidWeaponException, InvalidLevelException, InvalidArmorException {
        //Mage
        Weapon legendaryStaff = new Weapon("Staff A", 1, Weapon.WeaponType.Staff, 1);
        Armor legendaryHat = new Armor("Robetop A", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Head);
        Armor legendaryRobeTop = new Armor("Robetop A", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Body);
        Armor legendaryRobeBottoms = new Armor("Robetop A", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Legs);
        Hero mage = new Mage("魔法帝 - Mahōtei - Wizard King");
        mage.levelUp();
        mage.equipWeapon(legendaryStaff);
        mage.equipArmor(legendaryHat);
        mage.equipArmor(legendaryRobeTop);
        mage.equipArmor(legendaryRobeBottoms);
        System.out.println(mage.heroDisplay());

        //Rogue
        Weapon dragonDaggers = new Weapon("Dragon Dagger P++", 1, Weapon.WeaponType.Dagger, 1);
        Armor ghostlyHood = new Armor("Ghostly Hood", 1, Armor.ArmorType.Leather, new HeroAttribute(1, 10, 1), Item.Slot.Head);
        Armor ghostlyChest = new Armor("Ghostly Chest", 1, Armor.ArmorType.Leather, new HeroAttribute(1, 10, 1), Item.Slot.Body);
        Armor ghostlyLegs = new Armor("Ghostly Legs", 1, Armor.ArmorType.Leather, new HeroAttribute(1, 10, 1), Item.Slot.Legs);
        Hero rogue = new Rogue("Zoro Senpai");
        rogue.levelUp();
        rogue.levelUp();
        rogue.equipWeapon(dragonDaggers);
        rogue.equipArmor(ghostlyHood);
        rogue.equipArmor(ghostlyChest);
        rogue.equipArmor(ghostlyLegs);
        System.out.println(rogue.heroDisplay());

        //Warrior
        Weapon armadylGodsword = new Weapon("Armadyl Godsword", 1, Weapon.WeaponType.Sword, 8);
        Armor torvaFullHelm = new Armor("Torva Full Helm", 1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Head);
        Armor torvaPlateBody = new Armor("Torva Platebody", 1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Body);
        Armor torvaPlateLegs = new Armor("Torva Platelegs", 1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Legs);
        Hero warrior = new Warrior("Guts");
        warrior.levelUp();
        warrior.levelUp();
        warrior.levelUp();
        warrior.equipWeapon(armadylGodsword);
        warrior.equipArmor(torvaFullHelm);
        warrior.equipArmor(torvaPlateBody);
        warrior.equipArmor(torvaPlateLegs);
        System.out.println(warrior.heroDisplay());

        //Ranger
        Weapon twistedBow = new Weapon("Twisted Bow", 1, Weapon.WeaponType.Bow, 10);
        Armor masoriMask = new Armor("Masori Mask", 1, Armor.ArmorType.Leather, new HeroAttribute(1, 10, 1), Item.Slot.Head);
        Armor masoriBody = new Armor("Masori Body", 1, Armor.ArmorType.Leather, new HeroAttribute(1, 10, 1), Item.Slot.Body);
        Armor masoriChaps = new Armor("Masori Chaps", 1, Armor.ArmorType.Leather, new HeroAttribute(1, 10, 1), Item.Slot.Legs);
        Hero ranger = new Ranger("Soge King");

        //ranger.levelUp();
        ranger.equipWeapon(twistedBow);
        ranger.levelUp();
        ranger.levelUp();
        ranger.levelUp();
        ranger.levelUp();
        ranger.equipArmor(masoriMask);
        ranger.equipArmor(masoriBody);
        ranger.equipArmor(masoriChaps);
        System.out.println(ranger.heroDisplay());

    }
}
