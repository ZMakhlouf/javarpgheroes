import equipment.item.Item;
import equipment.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    public void create_WeaponShouldReturnCorrectName() {
        Weapon weapon = new Weapon("Twisted Bow", 1, Weapon.WeaponType.Bow, 10);
        // arrange
        String expectedName = "Twisted Bow";
        // act
        String actualName = weapon.getName();
        // assert
        assertEquals(expectedName, actualName);
    }

    @Test
    public void create_Weapon_ShouldReturnCorrectRequiredLevel() {
        Weapon weapon = new Weapon("Twisted Bow", 1, Weapon.WeaponType.Bow, 10);
        // arrange
        int expectedRequiredLevel = 1;
        // act
        int actualRequiredLevel = weapon.getRequiredLevel();
        // assert
        assertEquals(expectedRequiredLevel, actualRequiredLevel);
    }

    @Test
    public void create_Weapon_ShouldReturnCorrectSlot() {
        Weapon weapon = new Weapon("Twisted Bow", 1, Weapon.WeaponType.Bow, 10);

        assertEquals(Item.Slot.Weapon, weapon.getSlot());
    }

    @Test
    public void create_Weapon_shouldReturnCorrectWeaponType(){
        Weapon weapon = new Weapon("Twisted Bow", 1, Weapon.WeaponType.Bow, 10);

        assertEquals(Weapon.WeaponType.Bow, weapon.getWeaponType());
    }

    @Test
    public void create_Weapon_shouldReturnCorrectDamage(){
        Weapon weapon = new Weapon("Twisted Bow", 1, Weapon.WeaponType.Bow, 10);

        assertEquals(10, weapon.getWeaponDamage());
    }



}