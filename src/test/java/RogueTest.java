import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;
import exceptions.InvalidArmorException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;
import heroes.HeroAttribute;
import heroes.Ranger;
import heroes.Rogue;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }


    @Test
    public void create_Rogue_ShouldReturnCorrectNameLevelAndAttributes() {

        Rogue rogue = new Rogue("Ronin");

        // arrange
        String expectedName = "Ronin";
        // act
        String actualName = rogue.getName();
        // assert
        assertEquals(expectedName, actualName);

        // arrange
        int expectedLevel = 1;
        // act
        int actualLevel = rogue.getLevel();

        assertEquals(expectedLevel, actualLevel);

        // arrange
        int expectedStrength = 2;
        // act
        int actualStrength = rogue.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 6;
        // act
        int actualDexterity = rogue.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 1;
        // act
        int actualIntelligence = rogue.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void levelUp_Rogue_ShouldReturnCorrectIncrements() {
        Rogue rogue = new Rogue("Roninman");

        // arrange
        int expected = 2;
        // act
        rogue.levelUp();
        int actual = rogue.getLevel();
        // assert
        assertEquals(expected, actual);

        // Arrange
        int expectedStrength = 3;
        // Act
        int actualStrength = rogue.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 10;
        // act
        int actualDexterity = rogue.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 2;
        // act
        int actualIntelligence = rogue.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void equip_Weapon_ShouldThrowWeaponException() throws InvalidLevelException {
        Rogue rogue = new Rogue("Roninman");
        Weapon weapon = new Weapon("x", 1, Weapon.WeaponType.Axe, 0);

        try {
            rogue.equipWeapon(weapon);
            fail("It should fail at this point");
        } catch (InvalidWeaponException e) {

        }
    }

    @Test
    public void equip_Weapon_ShouldThrowLevelException() throws InvalidWeaponException {
        Rogue rogue = new Rogue("x");
        Weapon weapon = new Weapon("x", 99, Weapon.WeaponType.Sword, 0);

        try {
            rogue.equipWeapon(weapon);
            fail("It should fail at this point");
        } catch (InvalidLevelException e) {
        }
    }

    @Test
    public void equip_Armor_ShouldThrowArmorException() throws InvalidLevelException {
        Rogue rogue = new Rogue("x");
        Armor armor = new Armor("Bla", 0, Armor.ArmorType.Plate, new HeroAttribute(0, 0, 0), Item.Slot.Legs);

        try {
            rogue.equipArmor(armor);
            fail("It should fail at this point");
        } catch (InvalidArmorException e) {

        }
    }

    @Test
    public void equip_Armor_ShouldThrowLevelException() throws InvalidArmorException {
        Rogue rogue = new Rogue("x");
        Armor armor = new Armor("Bla", 10, Armor.ArmorType.Leather, new HeroAttribute(0, 0, 0), Item.Slot.Legs);

        try {
            rogue.equipArmor(armor);
            fail("It should fail at this point");
        } catch (InvalidLevelException e) {
        }
    }

    @Test
    public void equip_OnePieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Rogue rogue = new Rogue("x");
        Armor leather = new Armor("XX", 1, Armor.ArmorType.Leather, new HeroAttribute(7, 78, 9), Item.Slot.Body);

        rogue.equipArmor(leather);
        assertEquals(9, rogue.totalAttributes().getStrength());
        assertEquals(84, rogue.totalAttributes().getDexterity());
        assertEquals(10, rogue.totalAttributes().getIntelligence());

    }

    @Test
    public void equip_TwoPiecesOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Rogue rogue = new Rogue("x");
        Armor leather = new Armor("XX", 1, Armor.ArmorType.Leather, new HeroAttribute(7, 8000, 9), Item.Slot.Body);
        Armor mail = new Armor("XXX", 1, Armor.ArmorType.Mail, new HeroAttribute(7, 8875, 9), Item.Slot.Legs);

        rogue.equipArmor(leather);
        rogue.equipArmor(mail);
        assertEquals(16, rogue.totalAttributes().getStrength());
        assertEquals(16881, rogue.totalAttributes().getDexterity());
        assertEquals(19, rogue.totalAttributes().getIntelligence());

    }

    @Test
    public void equip_ReplacedPieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Rogue rogue = new Rogue("x");
        Armor piece = new Armor("piece", 1, Armor.ArmorType.Mail, new HeroAttribute(33, 88, 33), Item.Slot.Body);

        rogue.equipArmor(piece);
        assertEquals(35, rogue.totalAttributes().getStrength());
        assertEquals(94, rogue.totalAttributes().getDexterity());
        assertEquals(34, rogue.totalAttributes().getIntelligence());

        Armor replacedPiece = new Armor("replaced piece", 1, Armor.ArmorType.Leather, new HeroAttribute(1, 1, 1), Item.Slot.Body);
        rogue.equipArmor(replacedPiece);
        assertEquals(3, rogue.totalAttributes().getStrength());
        assertEquals(7, rogue.totalAttributes().getDexterity());
        assertEquals(2, rogue.totalAttributes().getIntelligence());
    }

    @Test
    public void equip_Weapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Rogue rogue = new Rogue("X");
        Weapon sword = new Weapon("Sword", 0, Weapon.WeaponType.Sword, 325);

        rogue.equipWeapon(sword);

        assertEquals(325 * (1 + (8 / 100)), sword.getWeaponDamage());
    }

    @Test
    public void equip_NoWeapon_ShouldReturnCorrectDamage() {
        Rogue rogue = new Rogue("X");

        assertEquals((1 * (1 + (6.00 / 100))), rogue.calculateDamage());
    }

    @Test
    public void equip_ReplacedWeapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Rogue rogue = new Rogue("X");
        Weapon sword = new Weapon("Sword", 0, Weapon.WeaponType.Sword, 43404);

        rogue.equipWeapon(sword);
        assertEquals(43404, sword.getWeaponDamage());

        Weapon replacedSword = new Weapon("replaced Sword", 1, Weapon.WeaponType.Sword, 6);
        rogue.equipWeapon(replacedSword);
        assertEquals(6, replacedSword.getWeaponDamage());
    }

    @Test
    public void equip_ArmorAndWeapon_ShouldReturnCorrectDamage() throws InvalidWeaponException, InvalidArmorException, InvalidLevelException {
        Rogue rogue = new Rogue("X");
        Weapon sword = new Weapon("XX", 0, Weapon.WeaponType.Sword, 5000);
        Armor head = new Armor("SteinBjorn Helmet", 0, Armor.ArmorType.Mail, new HeroAttribute(5, 5, 8), Item.Slot.Head);
        Armor body = new Armor("Bla", 0, Armor.ArmorType.Mail, new HeroAttribute(1, 1, 50), Item.Slot.Body);
        Armor legs = new Armor("Bla", 0, Armor.ArmorType.Leather, new HeroAttribute(3, 3, 500), Item.Slot.Legs);

        rogue.equipWeapon(sword);
        rogue.equipArmor(head);
        rogue.equipArmor(body);
        rogue.equipArmor(legs);

        assertEquals(5000 * (1 + (6 / 100)), sword.getWeaponDamage());
        assertEquals(11, rogue.totalAttributes().getStrength());
        assertEquals(15, rogue.totalAttributes().getDexterity());
        assertEquals(559, rogue.totalAttributes().getIntelligence());
    }

    @Test
    public void create_Mage_ShouldReturnCorrectDisplay(){
        Rogue rogue = new Rogue("Shadow");

        String expected = "Name: Shadow\n" +
                "Class: Rogue\n" +
                "Level: 1\n" +
                "Total Strength: 2\n" +
                "Total Dexterity: 6\n" +
                "Total Intelligence: 1\n" +
                "Total Damage: 1.06\n\n";

        assertEquals(expected, rogue.heroDisplay());
    }

}