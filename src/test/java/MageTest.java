import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;
import exceptions.InvalidArmorException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;
import heroes.Hero;
import heroes.HeroAttribute;
import heroes.Mage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MageTest {

    @Test
    public void create_MageShould_ReturnCorrectNameLevelAttributes() {
        Mage mage = new Mage("Voldemort");
        // arrange
        String expectedName = "Voldemort";
        // act
        String actualName = mage.getName();
        // assert
        assertEquals(expectedName, actualName);

        // arrange
        int expectedLevel = 1;
        // act
        int actualLevel = mage.getLevel();
        // assert
        assertEquals(expectedLevel, actualLevel);

        // arrange
        int expectedStrength = 1;
        // act
        int actualStrength = mage.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 1;
        // act
        int actualDexterity = mage.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 8;
        // act
        int actualIntelligence = mage.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void levelUp_Mage_ShouldReturnCorrectIncrements() {
        Mage mage = new Mage("Voldemort");
        // arrange
        int expectedLevelUp = 2;
        // act
        mage.levelUp();
        int actualLevelUp = mage.getLevel();
        // assert
        assertEquals(expectedLevelUp, actualLevelUp);

        // arrange
        int expectedStrength = 2;
        // act
        int actualStrength = mage.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 2;
        // act
        int actualDexterity = mage.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 13;
        // act
        int actualIntelligence = mage.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);

    }

    @Test
    public void equip_Weapon_ShouldThrowAppropriateWeaponException() throws InvalidLevelException {
        Mage mage = new Mage("Gandalf");
        Weapon weapon = new Weapon("Wooden Sword", 1, Weapon.WeaponType.Sword, 1);

        try {
            mage.equipWeapon(weapon);
            Assertions.fail("It should not be able to equip the weapon.");
        } catch (InvalidWeaponException e) {

        }
    }

    @Test
    public void equip_Weapon_ShouldThrowAppropriateWeaponLevelException() throws InvalidWeaponException {
        Mage mage = new Mage("Gandalf");
        Weapon weapon = new Weapon("Wooden Sword", 50, Weapon.WeaponType.Staff, 1);

        try {
            mage.equipWeapon(weapon);
            Assertions.fail("Your level is too low to equip this weapon.");
        } catch (InvalidLevelException e) {

        }
    }

    @Test
    public void equip_Armor_ShouldThrowAppropriateArmorException() throws InvalidLevelException {
        Mage mage = new Mage("Gandalf");
        Armor armor = new Armor("mail", 1, Armor.ArmorType.Mail, new HeroAttribute(10, 1, 1), Item.Slot.Head);

        try {
            mage.equipArmor(armor);
            Assertions.fail("You cannot equip this armortype, try cloth.");
        } catch (InvalidArmorException e) {

        }
    }

    @Test
    public void equip_Armor_ShouldThrowAppropriateArmorLevelException() throws InvalidArmorException {
        Mage mage = new Mage("Gandalf");
        Armor armor = new Armor("mail", 50, Armor.ArmorType.Mail, new HeroAttribute(10, 1, 1), Item.Slot.Head);

        try {
            mage.equipArmor(armor);
            Assertions.fail("You cannot equip this armortype, try cloth.");
        } catch (InvalidLevelException e) {

        }
    }

    @Test
    public void equip_OneArmorPiece_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Mage mage = new Mage("Kizaru");
        Armor cloth = new Armor("Headpiece", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Head);

        mage.equipArmor(cloth);
        assertEquals(2, mage.totalAttributes().getStrength());
        assertEquals(2, mage.totalAttributes().getDexterity());
        assertEquals(18, mage.totalAttributes().getIntelligence());
    }

    @Test
    public void equip_TwoArmorPieces_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Mage mage = new Mage("Kizaru");
        Armor head = new Armor("Headpiece", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Head);
        Armor body = new Armor("body", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Body);

        mage.equipArmor(head);
        mage.equipArmor(body);
        assertEquals(3, mage.totalAttributes().getStrength());
        assertEquals(3, mage.totalAttributes().getDexterity());
        assertEquals(28, mage.totalAttributes().getIntelligence());
    }

    @Test
    public void equip_ThreeArmorPieces_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Mage mage = new Mage("Kizaru");
        Armor head = new Armor("Headpiece", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Head);
        Armor body = new Armor("body", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Body);
        Armor legs = new Armor("body", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Legs);

        mage.equipArmor(head);
        mage.equipArmor(body);
        mage.equipArmor(legs);
        assertEquals(4, mage.totalAttributes().getStrength());
        assertEquals(4, mage.totalAttributes().getDexterity());
        assertEquals(38, mage.totalAttributes().getIntelligence());
    }

    @Test
    public void equip_ReplacedPieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Mage mage = new Mage("Kizaru");
        Armor head = new Armor("Headpiece", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Head);

        mage.equipArmor(head);
        assertEquals(2, mage.totalAttributes().getStrength());
        assertEquals(2, mage.totalAttributes().getDexterity());
        assertEquals(18, mage.totalAttributes().getIntelligence());

        Armor replacedHeadPiece = new Armor("Bandana", 1, Armor.ArmorType.Cloth, new HeroAttribute(2, 2, 20), Item.Slot.Head);
        mage.equipArmor(replacedHeadPiece);
        assertEquals(3, mage.totalAttributes().getStrength());
        assertEquals(3, mage.totalAttributes().getDexterity());
        assertEquals(28, mage.totalAttributes().getIntelligence());
    }

    @Test
    public void equip_Weapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Mage mage = new Mage("Wizzyy");
        Weapon staff = new Weapon("Shadow", 1, Weapon.WeaponType.Staff, 10);

        mage.equipWeapon(staff);

        assertEquals(10 * (1 + (8 / 100)), staff.getWeaponDamage());
    }

    @Test
    public void equip_NoWeapon_ShouldReturnCorrectDamage() {
        Mage mage = new Mage("Boohoo");

        assertEquals(1 * (1 + (8.00 / 100)), mage.calculateDamage());
    }

    @Test
    public void equip_ReplacedWeapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Mage mage = new Mage("Xo");
        Weapon staff = new Weapon("Shadow", 1, Weapon.WeaponType.Staff, 10);


        mage.equipWeapon(staff);
        assertEquals(10, staff.getWeaponDamage());

        Weapon replacedStaff = new Weapon("Replaced Staff", 1, Weapon.WeaponType.Staff, 20);
        mage.equipWeapon(replacedStaff);
        assertEquals(20, replacedStaff.getWeaponDamage());
    }

    @Test
    public void equip_WeaponAndArmor_ShouldReturnCorrectDamage() throws InvalidArmorException, InvalidLevelException, InvalidWeaponException {
        Mage mage = new Mage("Xo");
        Weapon staff = new Weapon("Shadow", 1, Weapon.WeaponType.Staff, 10);
        Armor legendaryHat = new Armor("Robetop A", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Head);
        Armor legendaryRobeTop = new Armor("Robetop A", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Body);
        Armor legendaryRobeBottoms = new Armor("Robetop A", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 10), Item.Slot.Legs);

        mage.equipWeapon(staff);
        mage.equipArmor(legendaryHat);
        mage.equipArmor(legendaryRobeTop);
        mage.equipArmor(legendaryRobeBottoms);

        assertEquals(10 * (1 + (8 / 100)), staff.getWeaponDamage());
        assertEquals(4, mage.totalAttributes().getStrength());
        assertEquals(4, mage.totalAttributes().getDexterity());
        assertEquals(38, mage.totalAttributes().getIntelligence());
    }


    @Test
    public void create_Mage_ShouldReturnCorrectDisplay(){
        Mage mage = new Mage("Kizaru");

        String expected = "Name: Kizaru\n" +
                "Class: Mage\n" +
                "Level: 1\n" +
                "Total Strength: 1\n" +
                "Total Dexterity: 1\n" +
                "Total Intelligence: 8\n" +
                "Total Damage: 1.08\n\n";

        assertEquals(expected, mage.heroDisplay());
    }


}