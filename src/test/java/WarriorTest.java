import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;
import exceptions.InvalidArmorException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;
import heroes.HeroAttribute;
import heroes.Ranger;
import heroes.Warrior;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }


    @Test
    public void create_Warrior_ShouldReturnCorrectNameLevelAndAttributes() {

        Warrior warrior = new Warrior("Guts");

        // arrange
        String expectedName = "Guts";
        // act
        String actualName = warrior.getName();
        // assert
        assertEquals(expectedName, actualName);

        // arrange
        int expectedLevel = 1;
        // act
        int actualLevel = warrior.getLevel();

        assertEquals(expectedLevel, actualLevel);

        // arrange
        int expectedStrength = 5;
        // act
        int actualStrength = warrior.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 2;
        // act
        int actualDexterity = warrior.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 1;
        // act
        int actualIntelligence = warrior.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void levelUp_Warrior_ShouldReturnCorrectIncrements() {
        Warrior warrior = new Warrior("Zoro");

        // arrange
        int expected = 2;
        // act
        warrior.levelUp();
        int actual = warrior.getLevel();
        // assert
        assertEquals(expected, actual);

        // Arrange
        int expectedStrength = 8;
        // Act
        int actualStrength = warrior.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 4;
        // act
        int actualDexterity = warrior.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 2;
        // act
        int actualIntelligence = warrior.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void equip_Weapon_ShouldThrowWeaponException() throws InvalidLevelException {
        Warrior warrior = new Warrior("Guts");
        Weapon weapon = new Weapon("AGS", 1, Weapon.WeaponType.Bow, 0);

        try {
            warrior.equipWeapon(weapon);
            fail("It should fail at this point");
        } catch (InvalidWeaponException e) {

        }
    }

    @Test
    public void equip_Weapon_ShouldThrowLevelException() throws InvalidWeaponException {
        Warrior warrior = new Warrior("x");
        Weapon weapon = new Weapon("x", 95, Weapon.WeaponType.Sword, 0);

        try {
            warrior.equipWeapon(weapon);
            fail("It should fail at this point");
        } catch (InvalidLevelException e) {
        }
    }

    @Test
    public void equip_Armor_ShouldThrowArmorException() throws InvalidLevelException {
        Warrior warrior = new Warrior("x");
        Armor armor = new Armor("Bla", 0, Armor.ArmorType.Cloth, new HeroAttribute(0, 0, 0), Item.Slot.Legs);

        try {
            warrior.equipArmor(armor);
            fail("It should fail at this point");
        } catch (InvalidArmorException e) {

        }
    }

    @Test
    public void equip_Armor_ShouldThrowLevelException() throws InvalidArmorException {
        Warrior warrior = new Warrior("x");
        Armor armor = new Armor("Bla", 85, Armor.ArmorType.Plate, new HeroAttribute(0, 0, 0), Item.Slot.Legs);

        try {
            warrior.equipArmor(armor);
            fail("It should fail at this point");
        } catch (InvalidLevelException e) {
        }
    }

    @Test
    public void equip_OnePieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Warrior warrior = new Warrior("x");
        Armor leather = new Armor("XX", 1, Armor.ArmorType.Mail, new HeroAttribute(10, 1, 1), Item.Slot.Body);

        warrior.equipArmor(leather);
        assertEquals(15, warrior.totalAttributes().getStrength());
        assertEquals(3, warrior.totalAttributes().getDexterity());
        assertEquals(2, warrior.totalAttributes().getIntelligence());

    }

    @Test
    public void equip_TwoPiecesOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Warrior warrior = new Warrior("x");
        Armor leather = new Armor("XX", 1, Armor.ArmorType.Mail, new HeroAttribute(10, 1, 1), Item.Slot.Body);
        Armor mail = new Armor("XXX", 1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Legs);

        warrior.equipArmor(leather);
        warrior.equipArmor(mail);
        assertEquals(25, warrior.totalAttributes().getStrength());
        assertEquals(4, warrior.totalAttributes().getDexterity());
        assertEquals(3, warrior.totalAttributes().getIntelligence());

    }

    @Test
    public void equip_ReplacedPieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Warrior warrior = new Warrior("x");
        Armor piece = new Armor("piece", 1, Armor.ArmorType.Mail, new HeroAttribute(10, 1, 1), Item.Slot.Body);

        warrior.equipArmor(piece);
        assertEquals(15, warrior.totalAttributes().getStrength());
        assertEquals(3, warrior.totalAttributes().getDexterity());
        assertEquals(2, warrior.totalAttributes().getIntelligence());

        Armor replacedPiece = new Armor("replaced piece", 1, Armor.ArmorType.Plate, new HeroAttribute(1, 2, 2), Item.Slot.Body);
        warrior.equipArmor(replacedPiece);
        assertEquals(6, warrior.totalAttributes().getStrength());
        assertEquals(4, warrior.totalAttributes().getDexterity());
        assertEquals(3, warrior.totalAttributes().getIntelligence());
    }

    @Test
    public void equip_Weapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Warrior warrior = new Warrior("X");
        Weapon sword = new Weapon("Sword", 0, Weapon.WeaponType.Sword, 10);

        warrior.equipWeapon(sword);

        assertEquals(10 * (1 + (8 / 100)), sword.getWeaponDamage());
    }

    @Test
    public void equip_NoWeapon_ShouldReturnCorrectDamage() {
        Warrior warrior = new Warrior("X");

        assertEquals((1 * (1 + (5.00 / 100))), warrior.calculateDamage());
    }

    @Test
    public void equip_ReplacedWeapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Warrior warrior = new Warrior("X");
        Weapon sword = new Weapon("Sword", 0, Weapon.WeaponType.Sword, 43404);

        warrior.equipWeapon(sword);
        assertEquals(43404, sword.getWeaponDamage());

        Weapon replacedSword = new Weapon("replaced Sword", 1, Weapon.WeaponType.Sword, 6);
        warrior.equipWeapon(replacedSword);
        assertEquals(6, replacedSword.getWeaponDamage());
    }

    @Test
    public void equip_ArmorAndWeapon_ShouldReturnCorrectDamage() throws InvalidWeaponException, InvalidArmorException, InvalidLevelException {
        Warrior warrior = new Warrior("X");
        Weapon sword = new Weapon("XX", 1, Weapon.WeaponType.Sword, 100);
        Armor head = new Armor("Helm", 0, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Head);
        Armor body = new Armor("Body", 0, Armor.ArmorType.Mail, new HeroAttribute(10, 1, 1), Item.Slot.Body);
        Armor legs = new Armor("Legs", 0, Armor.ArmorType.Mail, new HeroAttribute(10, 1, 1), Item.Slot.Legs);

        warrior.equipWeapon(sword);
        warrior.equipArmor(head);
        warrior.equipArmor(body);
        warrior.equipArmor(legs);

        assertEquals(100 * (1 + (5 / 100)), sword.getWeaponDamage());
        assertEquals(35, warrior.totalAttributes().getStrength());
        assertEquals(5, warrior.totalAttributes().getDexterity());
        assertEquals(4, warrior.totalAttributes().getIntelligence());
    }

    @Test
    public void create_Mage_ShouldReturnCorrectDisplay(){
        Warrior warrior = new Warrior("Shanks");

        String expected = "Name: Shanks\n" +
                "Class: Warrior\n" +
                "Level: 1\n" +
                "Total Strength: 5\n" +
                "Total Dexterity: 2\n" +
                "Total Intelligence: 1\n" +
                "Total Damage: 1.05\n\n";

        assertEquals(expected, warrior.heroDisplay());
    }

}