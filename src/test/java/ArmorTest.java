import equipment.item.Item;
import equipment.armor.Armor;
import heroes.HeroAttribute;
import heroes.Mage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    @Test
    public void create_Armor_ShouldReturnCorrectName() {
        Armor armor = new Armor("Torva Platebody",1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Body);

        assertEquals("Torva Platebody", armor.getName());
    }

    @Test
    public void create_Armor_ShouldReturnCorrectRequiredLevel() {
        Armor armor = new Armor("Torva Platebody",1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Body);

        assertEquals(1, armor.getRequiredLevel());
    }

    @Test
    public void create_Armor_ShouldReturnCorrectSlot() {
        Armor head = new Armor("Torva Fullhelm",1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Head);
        Armor body = new Armor("Torva Platebody",1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Body);
        Armor legs = new Armor("Torva Platelegs",1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Legs);

        assertEquals(Item.Slot.Head, head.getSlot());
        assertEquals(Item.Slot.Body, body.getSlot());
        assertEquals(Item.Slot.Legs, legs.getSlot());
    }

    @Test
    public void create_Armor_shouldReturnCorrectArmorTypes(){
        Armor plate = new Armor("Torva Fullhelm",1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Head);
        Armor leather = new Armor("leather",1, Armor.ArmorType.Leather, new HeroAttribute(10, 1, 1), Item.Slot.Head);
        Armor mail = new Armor("mail",1, Armor.ArmorType.Mail, new HeroAttribute(10, 1, 1), Item.Slot.Head);
        Armor cloth = new Armor("cloth",1, Armor.ArmorType.Cloth, new HeroAttribute(10, 1, 1), Item.Slot.Head);

        assertEquals(Armor.ArmorType.Plate, Armor.ArmorType.Plate);
        assertEquals(Armor.ArmorType.Leather, Armor.ArmorType.Leather);
        assertEquals(Armor.ArmorType.Mail, Armor.ArmorType.Mail);
        assertEquals(Armor.ArmorType.Cloth, Armor.ArmorType.Cloth);
    }

    @Test
    public void create_Armor_shouldReturnCorrectAttributes(){
        Armor armor = new Armor("Torva Fullhelm",1, Armor.ArmorType.Plate, new HeroAttribute(10, 1, 1), Item.Slot.Head);

        assertEquals(10, armor.getArmorAttribute().getStrength());
        assertEquals(1, armor.getArmorAttribute().getDexterity());
        assertEquals(1, armor.getArmorAttribute().getIntelligence());
    }

}