import equipment.armor.Armor;
import equipment.item.Item;
import equipment.weapon.Weapon;
import exceptions.InvalidArmorException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;
import heroes.HeroAttribute;
import heroes.Mage;
import heroes.Ranger;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void create_Ranger_ShouldReturnCorrectNameLevelAndAttributes() {

        Ranger ranger = new Ranger("Ranger");

        // arrange
        String expectedName = "Ranger";
        // act
        String actualName = ranger.getName();
        // assert
        assertEquals(expectedName, actualName);

        // arrange
        int expectedLevel = 1;
        // act
        int actualLevel = ranger.getLevel();

        assertEquals(expectedLevel, actualLevel);

        // arrange
        int expectedStrength = 1;
        // act
        int actualStrength = ranger.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 7;
        // act
        int actualDexterity = ranger.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 1;
        // act
        int actualIntelligence = ranger.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void levelUp_Ranger_ShouldReturnCorrectIncrements() {
        Ranger ranger = new Ranger("Rangerman");

        // arrange
        int expected = 2;
        // act
        ranger.levelUp();
        int actual = ranger.getLevel();
        // assert
        assertEquals(expected, actual);

        // Arrange
        int expectedStrength = 2;
        // Act
        int actualStrength = ranger.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 12;
        // act
        int actualDexterity = ranger.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 2;
        // act
        int actualIntelligence = ranger.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void equip_Weapon_ShouldThrowWeaponException() throws InvalidLevelException {
        Ranger ranger = new Ranger("Rangerman");
        Weapon weapon = new Weapon("x", 1, Weapon.WeaponType.Sword, 0);

        try {
            ranger.equipWeapon(weapon);
            fail("It should fail at this point");
        } catch (InvalidWeaponException e) {

        }
    }

    @Test
    public void equip_Weapon_ShouldThrowLevelException() throws InvalidWeaponException {
        Ranger ranger = new Ranger("x");
        Weapon weapon = new Weapon("x", 99, Weapon.WeaponType.Bow, 0);

        try {
            ranger.equipWeapon(weapon);
            fail("It should fail at this point");
        } catch (InvalidLevelException e) {
        }
    }

    @Test
    public void equip_Armor_ShouldThrowArmorException() throws InvalidLevelException {
        Ranger ranger = new Ranger("x");
        Armor armor = new Armor("Bla", 0, Armor.ArmorType.Plate, new HeroAttribute(0, 0, 0), Item.Slot.Legs);

        try {
            ranger.equipArmor(armor);
            fail("It should fail at this point");
        } catch (InvalidArmorException e) {

        }
    }

    @Test
    public void equip_Armor_ShouldThrowLevelException() throws InvalidArmorException {
        Ranger ranger = new Ranger("x");
        Armor armor = new Armor("Bla", 10, Armor.ArmorType.Plate, new HeroAttribute(0, 0, 0), Item.Slot.Legs);

        try {
            ranger.equipArmor(armor);
            fail("It should fail at this point");
        } catch (InvalidLevelException e) {
        }
    }

    @Test
    public void equip_OnePieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Ranger ranger = new Ranger("x");
        Armor leather = new Armor("XX", 1, Armor.ArmorType.Leather, new HeroAttribute(7, 78, 9), Item.Slot.Body);

        ranger.equipArmor(leather);
        assertEquals(8, ranger.totalAttributes().getStrength());
        assertEquals(85, ranger.totalAttributes().getDexterity());
        assertEquals(10, ranger.totalAttributes().getIntelligence());

    }

    @Test
    public void equip_TwoPiecesOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Ranger ranger = new Ranger("x");
        Armor leather = new Armor("XX", 1, Armor.ArmorType.Leather, new HeroAttribute(7, 8000, 9), Item.Slot.Body);
        Armor mail = new Armor("XXX", 1, Armor.ArmorType.Mail, new HeroAttribute(7, 8875, 9), Item.Slot.Legs);

        ranger.equipArmor(leather);
        ranger.equipArmor(mail);
        assertEquals(15, ranger.totalAttributes().getStrength());
        assertEquals(16882, ranger.totalAttributes().getDexterity());
        assertEquals(19, ranger.totalAttributes().getIntelligence());

    }

    @Test
    public void equip_ReplacedPieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Ranger ranger = new Ranger("x");
        Armor piece = new Armor("piece", 1, Armor.ArmorType.Mail, new HeroAttribute(33, 88, 33), Item.Slot.Body);

        ranger.equipArmor(piece);
        assertEquals(34, ranger.totalAttributes().getStrength());
        assertEquals(95, ranger.totalAttributes().getDexterity());
        assertEquals(34, ranger.totalAttributes().getIntelligence());

        Armor replacedPiece = new Armor("replaced piece", 1, Armor.ArmorType.Leather, new HeroAttribute(1, 1, 1), Item.Slot.Body);
        ranger.equipArmor(replacedPiece);
        assertEquals(2, ranger.totalAttributes().getStrength());
        assertEquals(8, ranger.totalAttributes().getDexterity());
        assertEquals(2, ranger.totalAttributes().getIntelligence());
    }

    @Test
    public void equip_Weapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Ranger ranger = new Ranger("X");
        Weapon bow = new Weapon("Bow", 0, Weapon.WeaponType.Bow, 325);

        ranger.equipWeapon(bow);

        assertEquals(325 * (1 + (8 / 100)), bow.getWeaponDamage());
    }

    @Test
    public void equip_NoWeapon_ShouldReturnCorrectDamage() {
        Ranger ranger = new Ranger("X");

        assertEquals((1 * (1 + (7.00 / 100))), ranger.calculateDamage());
    }

    @Test
    public void equip_ReplacedWeapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Ranger ranger = new Ranger("X");
        Weapon bow = new Weapon("Bow", 0, Weapon.WeaponType.Bow, 43404);

        ranger.equipWeapon(bow);
        assertEquals(43404, bow.getWeaponDamage());

        Weapon replacedBow = new Weapon("replaced Bow", 1, Weapon.WeaponType.Bow, 6);
        ranger.equipWeapon(replacedBow);
        assertEquals(6, replacedBow.getWeaponDamage());
    }

    @Test
    public void equip_ArmorAndWeapon_ShouldReturnCorrectDamage() throws InvalidWeaponException, InvalidArmorException, InvalidLevelException {
        Ranger ranger = new Ranger("X");
        Weapon bow = new Weapon("XX", 0, Weapon.WeaponType.Bow, 5000);
        Armor head = new Armor("SteinBjorn Helmet", 0, Armor.ArmorType.Mail, new HeroAttribute(5, 5, 8), Item.Slot.Head);
        Armor body = new Armor("Bla", 0, Armor.ArmorType.Mail, new HeroAttribute(1, 1, 50), Item.Slot.Body);
        Armor legs = new Armor("Bla", 0, Armor.ArmorType.Leather, new HeroAttribute(3, 3, 500), Item.Slot.Legs);

        ranger.equipWeapon(bow);
        ranger.equipArmor(head);
        ranger.equipArmor(body);
        ranger.equipArmor(legs);

        assertEquals(5000 * (1 + (8 / 100)), bow.getWeaponDamage());
        assertEquals(10, ranger.totalAttributes().getStrength());
        assertEquals(16, ranger.totalAttributes().getDexterity());
        assertEquals(559, ranger.totalAttributes().getIntelligence());
    }

    @Test
    public void create_Mage_ShouldReturnCorrectDisplay(){
        Ranger ranger = new Ranger("Usop");

        String expected = "Name: Usop\n" +
                "Class: Ranger\n" +
                "Level: 1\n" +
                "Total Strength: 1\n" +
                "Total Dexterity: 7\n" +
                "Total Intelligence: 1\n" +
                "Total Damage: 1.07\n\n";

        assertEquals(expected, ranger.heroDisplay());
    }

}